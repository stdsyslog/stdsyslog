<!--
SPDX-FileCopyrightText: Peter Pentchev
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the parse-stages project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.03.4] - 2024-06-30

### Fixes

- correct the syslog level in a manual page example

### Additions

- add a Python test suite that may be run in a container
- start some `MkDocs` documentation

### Other changes

- convert the changelog to the "Keep a Changelog" format
- switch to yearless copyright notices
- switch to SPDX copyright and license tags

## [0.03.3] - 2017-11-16

### Other changes

- bump the year on my copyright notice

## [0.03.2] - 2017-11-16

### Additions

- allow the install commands to be overridden
- handle the --help and --version long options

## [0.03.1] - 2015-08-04

### Fixes

- fix the syntax of the makedep.sh tool

### Additions

- use automake's tap-functions.sh shell function library to
  simplify the TAP tests
- add the 02-signals test

### Other changes

- drop the `_BSD_SOURCE` definition from the Makefile - not needed
  on modern systems, not sure why it was needed at all
- use C99's bool type for boolean flags
- remove some superfluous 'const' specifiers in prototypes
- remove the SIGCHLD handling, it did not change anything
- remove the SIGTERM special handling, too; it did not really work,
  it needed redesigning, and I'm not sure it's needed at all
- constify and/or reduce the scope of some variables
- use `puts()` instead of `printf("%s\n", ...)`
- use the C99 `%ju` and `%zu` printf() format specifiers

## [0.03] - 2013-02-19

### Additions

- add the `-p` command-line option to store the child process's
  ID into a file

### Other changes

- sprinkle `const` qualifiers around the code
- mark the `signal_pass` variable as static

## [0.02] - 2013-01-30

### Fixes

- add a `_XOPEN_SOURCE=700` definition for sigaction(2) on FreeBSD

### Other changes

- pass the HUP, INT, QUIT, ALRM, USR1, USR2, WINCH and TERM
  signals to the child process
- on a TERM signal, set a five-second timeout; on expiration,
  send a KILL signal and exit in five more seconds anyway
- use only the filename portion of the command as the log name
  (e.g. "sprog" instead of "/usr/bin/sprog")

## [0.01] - 2013-01-28

### Started

- first public release

[Unreleased]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.4...master
[0.03.4]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.3...release%2F0.03.4
[0.03.3]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.2...release%2F0.03.3
[0.03.2]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.1...release%2F0.03.2
[0.03.1]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03...release%2F0.03.1
[0.03]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.02...release%2F0.03
[0.02]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.01...release%2F0.02
[0.01]: https://gitlab.com/stdsyslog/stdsyslog/-/tags/release%2F0.01
