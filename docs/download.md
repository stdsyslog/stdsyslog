<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [stdsyslog](index.md) available for download.

## [0.03.4] - 2024-06-30

### Source tarball

- [stdsyslog-0.03.4.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.gz.asc))
- [stdsyslog-0.03.4.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.bz2.asc))
- [stdsyslog-0.03.4.tar.xz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.4.tar.xz.asc))

## [0.03.3] - 2017-11-16

### Source tarball

- [stdsyslog-0.03.3.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.gz.asc))
- [stdsyslog-0.03.3.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.bz2.asc))
- [stdsyslog-0.03.3.tar.xz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.3.tar.xz.asc))

## [0.03.2] - 2017-11-16

### Source tarball

- [stdsyslog-0.03.2.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.gz.asc))
- [stdsyslog-0.03.2.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.bz2.asc))
- [stdsyslog-0.03.2.tar.xz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.2.tar.xz.asc))

## [0.03.1] - 2015-08-04

### Source tarball

- [stdsyslog-0.03.1.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.1.tar.gz.asc))
- [stdsyslog-0.03.1.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.1.tar.bz2.asc))

## [0.03] - 2013-02-19

### Source tarball

- [stdsyslog-0.03.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.tar.gz.asc))
- [stdsyslog-0.03.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.03.tar.bz2.asc))

## [0.02] - 2013-01-30

### Source tarball

- [stdsyslog-0.02.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.02.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.02.tar.gz.asc))
- [stdsyslog-0.02.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.02.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.02.tar.bz2.asc))

## [0.01] - 2013-01-28

### Source tarball

- [stdsyslog-0.01.tar.gz](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.01.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.01.tar.gz.asc))
- [stdsyslog-0.01.tar.bz2](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.01.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/sys/stdsyslog/stdsyslog-0.01.tar.bz2.asc))

[0.03.4]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.3...release%2F0.03.4
[0.03.3]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.2...release%2F0.03.3
[0.03.2]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03.1...release%2F0.03.2
[0.03.1]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.03...release%2F0.03.1
[0.03]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.02...release%2F0.03
[0.02]: https://gitlab.com/stdsyslog/stdsyslog/-/compare/release%2F0.01...release%2F0.02
[0.01]: https://gitlab.com/stdsyslog/stdsyslog/-/tags/release%2F0.01
