<!--
SPDX-FileCopyrightText: Peter Pentchev
SPDX-License-Identifier: BSD-2-Clause
-->

# stdsyslog - log a program's output to the system log

\[[Home][ringlet-stdsyslog] | [GitLab][gitlab] | [[ReadTheDocs][readthedocs]\]

## Overview

The `stdsyslog` utility executes a program, reads everything that
it outputs to a set of file descriptors (by default the standard output
and standard error streams) and logs it to the system log.

## Usage

By default the `stdsyslog` utility executes the specified program and
logs every line output to the standard output stream as a `daemon.info`
message, and every line output to the standard error stream with
a `daemon.err` message.
The logging facility (`daemon` by default) may be specified using
the `-f` command-line option.

The mapping of file descriptors that the program outputs text on to
`syslog` levels may be modified using the `-d` command-line option;
the default is equivalent to `-d 1:info -d 2:err`.

If the `-p` command-line option is specified, `stdsyslog` will write
the child process's ID to the specified file.

## Examples

Log the string "information" to the `daemon:info` syslog facility:

``` sh
stdsyslog echo information
```

Log the string "error" to the `local4:err` facility:

``` sh
stdsyslog -f local4 sh -c 'echo error 1>&2'
```

Run the `sprog` program with two arguments, `some` and `args`,
store its process ID into the `sprog.pid` file and log the messages
appearing on its standard output to `daemon:info` and the ones on
its standard error stream to `daemon:err`:

``` sh
stdsyslog -p sprog.pid -d 1:info -d 2:crit sprog some args
```

## Contact

The `stdsyslog` utility was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-stdsyslog] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/stdsyslog/stdsyslog "The stdsyslog GitLab repository"
[readthedocs]: https://stdsyslog.readthedocs.io/ "The stdsyslog ReadTheDocs page"
[ringlet-stdsyslog]: https://devel.ringlet.net/sysutils/stdsyslog/ "The Ringlet stdsyslog homepage"
