<!--
SPDX-FileCopyrightText: Peter Pentchev
SPDX-License-Identifier: BSD-2-Clause
-->

# The stdsyslog test suite

## Overview

The `stdsyslog` source distribution contains a Python test suite that
attempts to run an instance of the `rsyslogd` tool to make sure that
`stdsyslog` invocations will produce syslog messages with the correct
facility and level.
The test suite is designed to be executed from a non-root user account in
an environment where passwordless `sudo` access is configured for that
user account.
There is also a harness for launching a Docker container, installing
the necessary Debian/Ubuntu packages within it, and then running
the test suite there.

## The test suite itself

The source of the `stdtestlog` suite is located in the `python/src/`
subdirectory of the `stdsyslog` source tree.
There is a `python/bin/stdtestlog` shell helper to run it.

Once a `stdsyslog` program has been compiled and passwordless `sudo`
access has been configured for a user account, the test suite is
invoked using the following command:

``` sh
stdtestlog run -p /path/to/stdsyslog
```

## Running the test suite in a Docker container

The `stdtestlog` runner may also be invoked as:

``` sh
stdtestlog container run [--prepared] [-i image] -p /path/to/stdsyslog
```

The `-i` option may be used to specify a container image other than
the `debian:unstable` default.
The `--prepared` option may be used to indicate that the Debian/Ubuntu
packages that the test suite requires (currently `python3`, `python3-click`,
`sudo`, and `rsyslog`) are already installed in the container image.

When invoked in this way, the runner will launch a Docker container,
mount its own Python source directory read-only within, mount the specified
`stdsyslog` program as `/usr/bin/stdsyslog`, configure passwordless `sudo`
access for the test account, and run the test suite.
