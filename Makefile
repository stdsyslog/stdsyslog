# SPDX-FileCopyrightText: Peter Pentchev
# SPDX-License-Identifier: BSD-2-Clause
#
# $Ringlet$

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
EXAMPLESDIR?=	${PREFIX}/share/examples/${PROG}

# TODO: regexp support for matching output lines to syslog specs
#PCRE_CPPFLAGS?=	-DHAVE_PCRE -I${LOCALBASE}/include
#PCRE_LIBS?=	-L${LOCALBASE}/lib -lpcre

CC?=		cc
CPPFLAGS?=
CPPFLAGS+=	${PCRE_CPPFLAGS}
CFLAGS?=		-g -pipe
CFLAGS+=	-Wall -W
LDFLAGS?=
LIBS?=

RM=		rm -f
MKDIR?=		mkdir -p
SETENV?=	env

PROG=		stdsyslog
OBJS=		stdsyslog.o
SRCS=		stdsyslog.c
DEPENDFILE=	.depend

#EXAMPLES=	t/t1.ini t/t2.ini
EXAMPLES=

MAN1=		stdsyslog.1
MAN1GZ=		${MAN1}.gz

BINOWN?=	root
BINGRP?=	wheel
BINMODE?=	555

MANOWN?=	${BINOWN}
MANGRP?=	${BINGRP}
MANMODE?=	644

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

# development/debugging flags, you may safely ignore them
BDECFLAGS=	-W -Wall -std=c99 -pedantic -Wbad-function-cast -Wcast-align \
		-Wcast-qual -Wchar-subscripts -Winline \
		-Wmissing-prototypes -Wnested-externs -Wpointer-arith \
		-Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings
CFLAGS+=	${BDECFLAGS}
CFLAGS+=	-Werror
#CFLAGS+=	-ggdb -g3

CPPFLAGS_STD?=	-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700
CPPFLAGS+=	${CPPFLAGS_STD}

INSTALL?=	install
COPY?=		-c
STRIP?=		-s

INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}
INSTALL_MAN?=		${INSTALL} ${COPY} -o ${MANOWN} -g ${MANGRP} -m ${MANMODE}

all:		${PROG} ${MAN1GZ}

clean:
		${RM} ${PROG} ${OBJS} ${MAN1GZ}

${PROG}:	${OBJS}
		${CC} ${LDFLAGS} -o ${PROG} ${OBJS} ${PCRE_LIBS}

.c.o:
		${CC} ${CPPFLAGS} ${CFLAGS} -c $<

${MAN1GZ}:	${MAN1}
		gzip -c9 ${MAN1} > ${MAN1GZ}.tmp
		mv ${MAN1GZ}.tmp ${MAN1GZ}

install:	all install-bin install-man

install-bin:
		-${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_PROGRAM} ${PROG} ${DESTDIR}${BINDIR}/

install-man:
		-${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_MAN} ${MAN1GZ} ${DESTDIR}${MANDIR}1/

install-examples:
		-${MKDIR} ${DESTDIR}${EXAMPLESDIR}
		${INSTALL_DATA} ${EXAMPLES} ${DESTDIR}${EXAMPLESDIR}/

depend:
		${SETENV} CPPFLAGS="-DSTDSYSLOG_MAKEDEP ${CPPFLAGS}" \
		DEPENDFILE="${DEPENDFILE}" sh makedep.sh ${SRCS}
		
test:		all
		prove t

.PHONY:		all clean depend install test

ifeq ($(wildcard ${DEPENDFILE}),${DEPENDFILE})
include ${DEPENDFILE}
endif
