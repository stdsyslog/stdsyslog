#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev
# SPDX-License-Identifier: CC0-1.0

[ -z "$DEPENDFILE" ] && DEPENDFILE=.depend
${CC-cc} $CPPFLAGS -MM "$@" > "$DEPENDFILE"
